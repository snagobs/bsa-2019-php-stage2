<?php

declare(strict_types=1);

namespace App\Task2;

class EmojiGenerator
{
    private $generator = ['🚀', '🚃', '🚄', '🚅', '🚇'];

    public function generate(): \Generator
    {
       foreach ($this->generator as $emoji) {
            yield $emoji;
        }

    }
}

