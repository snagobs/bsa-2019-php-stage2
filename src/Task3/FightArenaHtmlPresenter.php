<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\FightArena;

class FightArenaHtmlPresenter
{

    public function present(FightArena $arena): string
    {
        $result = '';
        foreach ($arena->all() as $fighter) {

            $result .=
                "<ul>" .
                "<li>Id : {$fighter->getId()}</li><br>" .
                "<li>{$fighter->getName()}: {$fighter->getHealth()}, {$fighter->getAttack()}</li><br>" .
                "<li>Image >>> <img src=\"{$fighter->getImage()}\"></li><br>" .
                "</ul><hr>" .
                "<br>";

        }
        return $result;

    }
}
