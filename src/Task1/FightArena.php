<?php

declare(strict_types=1);

namespace App\Task1;

class FightArena
{
    private $fighters;

    public function add(Fighter $fighter): void
    {
        $this->fighters[] = $fighter;
    }

    public function mostPowerful(): Fighter
    {
        $mostPower = $this->fighters[0];

        for ($i = 1; $i < count($this->fighters); $i++) {
            if ($this->fighters[$i]->getAttack() > $mostPower->getAttack()) {
                $mostPower = $this->fighters[$i];
            }
            return $mostPower;
        }
    }

    public function mostHealthy(): Fighter
    {
        $mostHealth = $this->fighters[0];

        for ($i = 1; $i < count($this->fighters); $i++) {
            if ($this->fighters[$i]->getHealth() > $mostHealth->getHealth()) {
                $mostHealth = $this->fighters[$i];
            }
            return $mostHealth;
        }

    }

    public function all(): array
    {
        return $this->fighters;
    }

}
